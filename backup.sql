PGDMP         4                x           lougeh_supermarket    12.3    12.3 f    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16393    lougeh_supermarket    DATABASE     �   CREATE DATABASE lougeh_supermarket WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
 "   DROP DATABASE lougeh_supermarket;
                postgres    false                        3079    25190    pgcrypto 	   EXTENSION     <   CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;
    DROP EXTENSION pgcrypto;
                   false            �           0    0    EXTENSION pgcrypto    COMMENT     <   COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';
                        false    2            �            1259    16978    cart    TABLE        CREATE TABLE public.cart (
    id_no integer NOT NULL,
    name character varying(255),
    lastname character varying(255)
);
    DROP TABLE public.cart;
       public         heap    postgres    false            �            1259    16976    cart_id_no_seq    SEQUENCE     �   CREATE SEQUENCE public.cart_id_no_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.cart_id_no_seq;
       public          postgres    false    209            �           0    0    cart_id_no_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.cart_id_no_seq OWNED BY public.cart.id_no;
          public          postgres    false    208            �            1259    17052    delivery_transactions    TABLE       CREATE TABLE public.delivery_transactions (
    dtransactions_code integer NOT NULL,
    dr_no integer,
    supplier_id integer NOT NULL,
    dtransaction_date date,
    total_cost numeric,
    created_at date,
    created_by integer,
    updated_by integer,
    updated_at date
);
 )   DROP TABLE public.delivery_transactions;
       public         heap    postgres    false            �            1259    17050 0   delivery_transactions_				dtransactions_code_seq    SEQUENCE     �   CREATE SEQUENCE public."delivery_transactions_				dtransactions_code_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 I   DROP SEQUENCE public."delivery_transactions_				dtransactions_code_seq";
       public          postgres    false    214            �           0    0 0   delivery_transactions_				dtransactions_code_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."delivery_transactions_				dtransactions_code_seq" OWNED BY public.delivery_transactions.dtransactions_code;
          public          postgres    false    213            �            1259    25169    delivery_transactions_items    TABLE     �   CREATE TABLE public.delivery_transactions_items (
    dti_id integer NOT NULL,
    dt_no integer,
    barcode integer,
    product_description character varying(255),
    unit_cost numeric,
    quantity numeric
);
 /   DROP TABLE public.delivery_transactions_items;
       public         heap    postgres    false            �            1259    25167 7   delivery_transactions_items_dtransaction_items_code_seq    SEQUENCE     �   CREATE SEQUENCE public.delivery_transactions_items_dtransaction_items_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 N   DROP SEQUENCE public.delivery_transactions_items_dtransaction_items_code_seq;
       public          postgres    false    227            �           0    0 7   delivery_transactions_items_dtransaction_items_code_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.delivery_transactions_items_dtransaction_items_code_seq OWNED BY public.delivery_transactions_items.dti_id;
          public          postgres    false    226            �            1259    25134 %   delivery_transactions_supplier_id_seq    SEQUENCE     �   CREATE SEQUENCE public.delivery_transactions_supplier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.delivery_transactions_supplier_id_seq;
       public          postgres    false    214            �           0    0 %   delivery_transactions_supplier_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.delivery_transactions_supplier_id_seq OWNED BY public.delivery_transactions.supplier_id;
          public          postgres    false    225            �            1259    25107 	   inventory    TABLE     Q  CREATE TABLE public.inventory (
    inventory_id integer NOT NULL,
    barcode integer,
    product_description character varying(255),
    quantity numeric,
    unit_cost numeric,
    created_by integer,
    created_at timestamp with time zone,
    updated_by integer,
    updated_at timestamp with time zone,
    sales_cost numeric
);
    DROP TABLE public.inventory;
       public         heap    postgres    false            �            1259    16470 
   inventory2    TABLE     �   CREATE TABLE public.inventory2 (
    inventory_code integer NOT NULL,
    barcode character varying(255) NOT NULL,
    product_description character varying(255) NOT NULL,
    quantity integer NOT NULL
);
    DROP TABLE public.inventory2;
       public         heap    postgres    false            �            1259    16468    inventory2_inventory_code_seq    SEQUENCE     �   CREATE SEQUENCE public.inventory2_inventory_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.inventory2_inventory_code_seq;
       public          postgres    false    204            �           0    0    inventory2_inventory_code_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.inventory2_inventory_code_seq OWNED BY public.inventory2.inventory_code;
          public          postgres    false    203            �            1259    25105    inventory_inventory_code_seq    SEQUENCE     �   CREATE SEQUENCE public.inventory_inventory_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.inventory_inventory_code_seq;
       public          postgres    false    221            �           0    0    inventory_inventory_code_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.inventory_inventory_code_seq OWNED BY public.inventory.inventory_id;
          public          postgres    false    220            �            1259    17005    roles    TABLE     �   CREATE TABLE public.roles (
    role_id integer NOT NULL,
    role_name character varying(255),
    created_at date,
    created_by integer,
    updated_by integer,
    updated_at date
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    25120    sales_transactions    TABLE     |  CREATE TABLE public.sales_transactions (
    st_id integer NOT NULL,
    or_no integer NOT NULL,
    stransaction_date timestamp with time zone,
    total_cost numeric,
    payment_amt numeric,
    customer_name character varying(255),
    customer_address character varying(255),
    customer_contact_no bigint,
    created_at timestamp with time zone,
    created_by integer
);
 &   DROP TABLE public.sales_transactions;
       public         heap    postgres    false            �            1259    16613    sales_transactions_items    TABLE     �   CREATE TABLE public.sales_transactions_items (
    sti_id integer NOT NULL,
    or_no integer NOT NULL,
    barcode integer,
    product_description character varying(255),
    quantity numeric,
    sales_cost numeric
);
 ,   DROP TABLE public.sales_transactions_items;
       public         heap    postgres    false            �            1259    17158 "   sales_transactions_items_or_no_seq    SEQUENCE     �   CREATE SEQUENCE public.sales_transactions_items_or_no_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.sales_transactions_items_or_no_seq;
       public          postgres    false    206            �           0    0 "   sales_transactions_items_or_no_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.sales_transactions_items_or_no_seq OWNED BY public.sales_transactions_items.or_no;
          public          postgres    false    219            �            1259    16611 3   sales_transactions_items_stransaction_item_code_seq    SEQUENCE     �   CREATE SEQUENCE public.sales_transactions_items_stransaction_item_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 J   DROP SEQUENCE public.sales_transactions_items_stransaction_item_code_seq;
       public          postgres    false    206            �           0    0 3   sales_transactions_items_stransaction_item_code_seq    SEQUENCE OWNED BY     {   ALTER SEQUENCE public.sales_transactions_items_stransaction_item_code_seq OWNED BY public.sales_transactions_items.sti_id;
          public          postgres    false    205            �            1259    25118    sales_transactions_or_no_seq    SEQUENCE     �   CREATE SEQUENCE public.sales_transactions_or_no_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.sales_transactions_or_no_seq;
       public          postgres    false    224            �           0    0    sales_transactions_or_no_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.sales_transactions_or_no_seq OWNED BY public.sales_transactions.or_no;
          public          postgres    false    223            �            1259    25116 )   sales_transactions_stransactions_code_seq    SEQUENCE     �   CREATE SEQUENCE public.sales_transactions_stransactions_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.sales_transactions_stransactions_code_seq;
       public          postgres    false    224            �           0    0 )   sales_transactions_stransactions_code_seq    SEQUENCE OWNED BY     j   ALTER SEQUENCE public.sales_transactions_stransactions_code_seq OWNED BY public.sales_transactions.st_id;
          public          postgres    false    222            �            1259    16908    stock_availability    TABLE     l   CREATE TABLE public.stock_availability (
    product_id integer NOT NULL,
    available boolean NOT NULL
);
 &   DROP TABLE public.stock_availability;
       public         heap    postgres    false            �            1259    17080 	   suppliers    TABLE       CREATE TABLE public.suppliers (
    supplier_id integer NOT NULL,
    company_name character varying(255),
    contact_no bigint,
    company_address character varying(255),
    status boolean,
    created_at date,
    created_by integer,
    updated_by integer,
    updated_at date
);
    DROP TABLE public.suppliers;
       public         heap    postgres    false            �            1259    17109    suppliers_supplier_code_seq    SEQUENCE     �   CREATE SEQUENCE public.suppliers_supplier_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.suppliers_supplier_code_seq;
       public          postgres    false    215            �           0    0    suppliers_supplier_code_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.suppliers_supplier_code_seq OWNED BY public.suppliers.supplier_id;
          public          postgres    false    218            �            1259    16989    test    TABLE     �   CREATE TABLE public.test (
    name_id integer NOT NULL,
    school character varying(255),
    address character varying(255),
    id_no integer
);
    DROP TABLE public.test;
       public         heap    postgres    false            �            1259    25250    test1    TABLE     O   CREATE TABLE public.test1 (
    user_id integer NOT NULL,
    enc_pass text
);
    DROP TABLE public.test1;
       public         heap    postgres    false            �            1259    25248    test1_id_seq    SEQUENCE     �   CREATE SEQUENCE public.test1_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.test1_id_seq;
       public          postgres    false    229            �           0    0    test1_id_seq    SEQUENCE OWNED BY     B   ALTER SEQUENCE public.test1_id_seq OWNED BY public.test1.user_id;
          public          postgres    false    228            �            1259    16987    test_name_id_seq    SEQUENCE     �   CREATE SEQUENCE public.test_name_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.test_name_id_seq;
       public          postgres    false    211            �           0    0    test_name_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.test_name_id_seq OWNED BY public.test.name_id;
          public          postgres    false    210            �            1259    17100    users    TABLE     E  CREATE TABLE public.users (
    user_id integer NOT NULL,
    employee_code integer,
    username character varying(255),
    password character varying(255),
    role_id integer,
    created_at timestamp with time zone,
    created_by integer,
    updated_by integer,
    updated_at date,
    salt character varying(255)
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    17098    users_users_code_seq    SEQUENCE     �   CREATE SEQUENCE public.users_users_code_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.users_users_code_seq;
       public          postgres    false    217            �           0    0    users_users_code_seq    SEQUENCE OWNED BY     J   ALTER SEQUENCE public.users_users_code_seq OWNED BY public.users.user_id;
          public          postgres    false    216            �
           2604    16981 
   cart id_no    DEFAULT     h   ALTER TABLE ONLY public.cart ALTER COLUMN id_no SET DEFAULT nextval('public.cart_id_no_seq'::regclass);
 9   ALTER TABLE public.cart ALTER COLUMN id_no DROP DEFAULT;
       public          postgres    false    209    208    209            �
           2604    17055 (   delivery_transactions dtransactions_code    DEFAULT     �   ALTER TABLE ONLY public.delivery_transactions ALTER COLUMN dtransactions_code SET DEFAULT nextval('public."delivery_transactions_				dtransactions_code_seq"'::regclass);
 W   ALTER TABLE public.delivery_transactions ALTER COLUMN dtransactions_code DROP DEFAULT;
       public          postgres    false    214    213    214            �
           2604    25136 !   delivery_transactions supplier_id    DEFAULT     �   ALTER TABLE ONLY public.delivery_transactions ALTER COLUMN supplier_id SET DEFAULT nextval('public.delivery_transactions_supplier_id_seq'::regclass);
 P   ALTER TABLE public.delivery_transactions ALTER COLUMN supplier_id DROP DEFAULT;
       public          postgres    false    225    214                       2604    25172 "   delivery_transactions_items dti_id    DEFAULT     �   ALTER TABLE ONLY public.delivery_transactions_items ALTER COLUMN dti_id SET DEFAULT nextval('public.delivery_transactions_items_dtransaction_items_code_seq'::regclass);
 Q   ALTER TABLE public.delivery_transactions_items ALTER COLUMN dti_id DROP DEFAULT;
       public          postgres    false    226    227    227                       2604    25110    inventory inventory_id    DEFAULT     �   ALTER TABLE ONLY public.inventory ALTER COLUMN inventory_id SET DEFAULT nextval('public.inventory_inventory_code_seq'::regclass);
 E   ALTER TABLE public.inventory ALTER COLUMN inventory_id DROP DEFAULT;
       public          postgres    false    221    220    221            �
           2604    16473    inventory2 inventory_code    DEFAULT     �   ALTER TABLE ONLY public.inventory2 ALTER COLUMN inventory_code SET DEFAULT nextval('public.inventory2_inventory_code_seq'::regclass);
 H   ALTER TABLE public.inventory2 ALTER COLUMN inventory_code DROP DEFAULT;
       public          postgres    false    203    204    204                       2604    25123    sales_transactions st_id    DEFAULT     �   ALTER TABLE ONLY public.sales_transactions ALTER COLUMN st_id SET DEFAULT nextval('public.sales_transactions_stransactions_code_seq'::regclass);
 G   ALTER TABLE public.sales_transactions ALTER COLUMN st_id DROP DEFAULT;
       public          postgres    false    224    222    224                       2604    25124    sales_transactions or_no    DEFAULT     �   ALTER TABLE ONLY public.sales_transactions ALTER COLUMN or_no SET DEFAULT nextval('public.sales_transactions_or_no_seq'::regclass);
 G   ALTER TABLE public.sales_transactions ALTER COLUMN or_no DROP DEFAULT;
       public          postgres    false    224    223    224            �
           2604    16616    sales_transactions_items sti_id    DEFAULT     �   ALTER TABLE ONLY public.sales_transactions_items ALTER COLUMN sti_id SET DEFAULT nextval('public.sales_transactions_items_stransaction_item_code_seq'::regclass);
 N   ALTER TABLE public.sales_transactions_items ALTER COLUMN sti_id DROP DEFAULT;
       public          postgres    false    206    205    206            �
           2604    17160    sales_transactions_items or_no    DEFAULT     �   ALTER TABLE ONLY public.sales_transactions_items ALTER COLUMN or_no SET DEFAULT nextval('public.sales_transactions_items_or_no_seq'::regclass);
 M   ALTER TABLE public.sales_transactions_items ALTER COLUMN or_no DROP DEFAULT;
       public          postgres    false    219    206                        2604    17111    suppliers supplier_id    DEFAULT     �   ALTER TABLE ONLY public.suppliers ALTER COLUMN supplier_id SET DEFAULT nextval('public.suppliers_supplier_code_seq'::regclass);
 D   ALTER TABLE public.suppliers ALTER COLUMN supplier_id DROP DEFAULT;
       public          postgres    false    218    215            �
           2604    16992    test name_id    DEFAULT     l   ALTER TABLE ONLY public.test ALTER COLUMN name_id SET DEFAULT nextval('public.test_name_id_seq'::regclass);
 ;   ALTER TABLE public.test ALTER COLUMN name_id DROP DEFAULT;
       public          postgres    false    211    210    211                       2604    25253    test1 user_id    DEFAULT     i   ALTER TABLE ONLY public.test1 ALTER COLUMN user_id SET DEFAULT nextval('public.test1_id_seq'::regclass);
 <   ALTER TABLE public.test1 ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    228    229    229                       2604    17103    users user_id    DEFAULT     q   ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_users_code_seq'::regclass);
 <   ALTER TABLE public.users ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    217    216    217            �          0    16978    cart 
   TABLE DATA           5   COPY public.cart (id_no, name, lastname) FROM stdin;
    public          postgres    false    209   �|       �          0    17052    delivery_transactions 
   TABLE DATA           �   COPY public.delivery_transactions (dtransactions_code, dr_no, supplier_id, dtransaction_date, total_cost, created_at, created_by, updated_by, updated_at) FROM stdin;
    public          postgres    false    214   }       �          0    25169    delivery_transactions_items 
   TABLE DATA           w   COPY public.delivery_transactions_items (dti_id, dt_no, barcode, product_description, unit_cost, quantity) FROM stdin;
    public          postgres    false    227   �}       �          0    25107 	   inventory 
   TABLE DATA           �   COPY public.inventory (inventory_id, barcode, product_description, quantity, unit_cost, created_by, created_at, updated_by, updated_at, sales_cost) FROM stdin;
    public          postgres    false    221   �~       �          0    16470 
   inventory2 
   TABLE DATA           \   COPY public.inventory2 (inventory_code, barcode, product_description, quantity) FROM stdin;
    public          postgres    false    204   π       �          0    17005    roles 
   TABLE DATA           c   COPY public.roles (role_id, role_name, created_at, created_by, updated_by, updated_at) FROM stdin;
    public          postgres    false    212   a�       �          0    25120    sales_transactions 
   TABLE DATA           �   COPY public.sales_transactions (st_id, or_no, stransaction_date, total_cost, payment_amt, customer_name, customer_address, customer_contact_no, created_at, created_by) FROM stdin;
    public          postgres    false    224          �          0    16613    sales_transactions_items 
   TABLE DATA           u   COPY public.sales_transactions_items (sti_id, or_no, barcode, product_description, quantity, sales_cost) FROM stdin;
    public          postgres    false    206   -�       �          0    16908    stock_availability 
   TABLE DATA           C   COPY public.stock_availability (product_id, available) FROM stdin;
    public          postgres    false    207   ^�       �          0    17080 	   suppliers 
   TABLE DATA           �   COPY public.suppliers (supplier_id, company_name, contact_no, company_address, status, created_at, created_by, updated_by, updated_at) FROM stdin;
    public          postgres    false    215   {�       �          0    16989    test 
   TABLE DATA           ?   COPY public.test (name_id, school, address, id_no) FROM stdin;
    public          postgres    false    211   <�       �          0    25250    test1 
   TABLE DATA           2   COPY public.test1 (user_id, enc_pass) FROM stdin;
    public          postgres    false    229   ��       �          0    17100    users 
   TABLE DATA           �   COPY public.users (user_id, employee_code, username, password, role_id, created_at, created_by, updated_by, updated_at, salt) FROM stdin;
    public          postgres    false    217   ��       �           0    0    cart_id_no_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.cart_id_no_seq', 20, true);
          public          postgres    false    208            �           0    0 0   delivery_transactions_				dtransactions_code_seq    SEQUENCE SET     a   SELECT pg_catalog.setval('public."delivery_transactions_				dtransactions_code_seq"', 96, true);
          public          postgres    false    213            �           0    0 7   delivery_transactions_items_dtransaction_items_code_seq    SEQUENCE SET     f   SELECT pg_catalog.setval('public.delivery_transactions_items_dtransaction_items_code_seq', 91, true);
          public          postgres    false    226            �           0    0 %   delivery_transactions_supplier_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public.delivery_transactions_supplier_id_seq', 18, true);
          public          postgres    false    225            �           0    0    inventory2_inventory_code_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.inventory2_inventory_code_seq', 11, true);
          public          postgres    false    203            �           0    0    inventory_inventory_code_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.inventory_inventory_code_seq', 32, true);
          public          postgres    false    220            �           0    0 "   sales_transactions_items_or_no_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.sales_transactions_items_or_no_seq', 63, true);
          public          postgres    false    219            �           0    0 3   sales_transactions_items_stransaction_item_code_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.sales_transactions_items_stransaction_item_code_seq', 185, true);
          public          postgres    false    205            �           0    0    sales_transactions_or_no_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.sales_transactions_or_no_seq', 38, true);
          public          postgres    false    223            �           0    0 )   sales_transactions_stransactions_code_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.sales_transactions_stransactions_code_seq', 119, true);
          public          postgres    false    222            �           0    0    suppliers_supplier_code_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.suppliers_supplier_code_seq', 20, true);
          public          postgres    false    218            �           0    0    test1_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.test1_id_seq', 3, true);
          public          postgres    false    228            �           0    0    test_name_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.test_name_id_seq', 17, true);
          public          postgres    false    210            �           0    0    users_users_code_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.users_users_code_seq', 32, true);
          public          postgres    false    216                       2606    16986    cart cart_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY public.cart
    ADD CONSTRAINT cart_pkey PRIMARY KEY (id_no);
 8   ALTER TABLE ONLY public.cart DROP CONSTRAINT cart_pkey;
       public            postgres    false    209                       2606    25177 <   delivery_transactions_items delivery_transactions_items_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.delivery_transactions_items
    ADD CONSTRAINT delivery_transactions_items_pkey PRIMARY KEY (dti_id);
 f   ALTER TABLE ONLY public.delivery_transactions_items DROP CONSTRAINT delivery_transactions_items_pkey;
       public            postgres    false    227                       2606    17057 0   delivery_transactions delivery_transactions_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.delivery_transactions
    ADD CONSTRAINT delivery_transactions_pkey PRIMARY KEY (dtransactions_code);
 Z   ALTER TABLE ONLY public.delivery_transactions DROP CONSTRAINT delivery_transactions_pkey;
       public            postgres    false    214                       2606    16478    inventory2 inventory2_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.inventory2
    ADD CONSTRAINT inventory2_pkey PRIMARY KEY (inventory_code);
 D   ALTER TABLE ONLY public.inventory2 DROP CONSTRAINT inventory2_pkey;
       public            postgres    false    204                       2606    25115    inventory inventory_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (inventory_id);
 B   ALTER TABLE ONLY public.inventory DROP CONSTRAINT inventory_pkey;
       public            postgres    false    221                       2606    25284    roles roles_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (role_id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    212            
           2606    16618 6   sales_transactions_items sales_transactions_items_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.sales_transactions_items
    ADD CONSTRAINT sales_transactions_items_pkey PRIMARY KEY (sti_id);
 `   ALTER TABLE ONLY public.sales_transactions_items DROP CONSTRAINT sales_transactions_items_pkey;
       public            postgres    false    206                       2606    25129 *   sales_transactions sales_transactions_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.sales_transactions
    ADD CONSTRAINT sales_transactions_pkey PRIMARY KEY (st_id);
 T   ALTER TABLE ONLY public.sales_transactions DROP CONSTRAINT sales_transactions_pkey;
       public            postgres    false    224                       2606    16912 *   stock_availability stock_availability_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.stock_availability
    ADD CONSTRAINT stock_availability_pkey PRIMARY KEY (product_id);
 T   ALTER TABLE ONLY public.stock_availability DROP CONSTRAINT stock_availability_pkey;
       public            postgres    false    207                       2606    17125    suppliers suppliers_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.suppliers
    ADD CONSTRAINT suppliers_pkey PRIMARY KEY (supplier_id);
 B   ALTER TABLE ONLY public.suppliers DROP CONSTRAINT suppliers_pkey;
       public            postgres    false    215                        2606    25258    test1 test1_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.test1
    ADD CONSTRAINT test1_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.test1 DROP CONSTRAINT test1_pkey;
       public            postgres    false    229                       2606    16997    test test_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_pkey PRIMARY KEY (name_id);
 8   ALTER TABLE ONLY public.test DROP CONSTRAINT test_pkey;
       public            postgres    false    211                       2606    17108    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    217            !           2606    16998    test test_id_no_fkey    FK CONSTRAINT     }   ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_id_no_fkey FOREIGN KEY (id_no) REFERENCES public.cart(id_no) NOT VALID;
 >   ALTER TABLE ONLY public.test DROP CONSTRAINT test_id_no_fkey;
       public          postgres    false    209    2830    211            �   "   x�3�,��J�����2B�#sL�91z\\\ �p�      �   l   x�����@CϦ���D4���:�F�(�������{k!�"�
>��
'� V4��,X�J]���㪗q�Na���� 8��z�!w೛�+a�r�����<      �   Z  x���=n�0Fg�޺�(R����Y�D�A���!�/��E�؉��0��DK@G�L�;�Pm��!����s_�~��d�ָ�e����b�qlN��������,d�aC�.�����(�=�</v
�rփۯW���p�2a��ޛ\U]�w���Dا�0I/�i����ն^��z:�v��vC I�o�Q��e��u��{-v�	t���l���Z`�@���d���� G�>������m��h�gj`��&�4��I>����4ݎ�U�v��'t��4vt���4�:��k���e�^g`-_~����*+={��*��E���N���Ƙot��      �   �  x���O��0���O��F��v�J�j�[�y/��ۖ���P��LL��Xs{z����#Ԍ>^��b�m��zȀ���n��8�f�b��k��k�hj�;�?�>�?������b^5 /�-b*3:��ɖ\�:�0*���jD	��s�Ӷ���\��&�$ -�M���!��p�F�fI���N#@�Hp�>�W���t����#������£�xC5�v��~��{f�R9��&��:N�(�A�1EX�/}��vyаt�A�)�+?5+�X�7"c^E�a���ns;t-�f|s�8$r�h��HN���������?�OZNy�bI�^7Z%rJX��/�;
;h���X��c]Kh,/��m�)��Hb�:�%�R�@����؝�GP�~��e3�ˁ��Dƿe��bjB>�����w��8��w��#��N����R�����Q��      �   �   x�����0E��L�b;�p�BoH���Hm��x�����%�aE^%�q(�4~��W����!��-5g�\��A�x���G G��=�Zo]�v���\��x��|�)�F����i�����Y�      �   Q   x�3�LL����4202�50�50�445�4�
X�rq&'gd��Ue̙\Z\������4��҂Ԣ���|���qqq �k!�      �   [  x��ѽN�0 �������?����*H,]�R��#%A<=vi
�f�t�#��@�x�UΉqn��$�Q�8/8<�޿쮭߶p���_���� Z�*ue8�cd4ǖ�(�A]v�ȼl���M�X���s�\:j����Hste
}��&����Az�v�͸�5[���]xs�|c@09Cm%ZI� T��P`���k�~�f��M���1[�!O���C��Vɔ�J���0�М֨J5W���7���*��B'V�TF0N�0¨����/.SS�{��Ak%����ߤ�1W""��o�M��*s*Ifu�TVUW�SI2M4�|���W�S��VE�e_�a��      �   !  x����n�0���S���!J"%�t���˰�.i��Y:{��ӏr�ASY>	每&��3,��Wۺ��O1��7H�tJ��.v��붛؃���ʤZe�
0_����C��('����0��r#����&��Vd>4\؉D�YN��FC�zۡkE�J�� YB}���o~,��$�@�% �#�s#�
YBf	����h��A���K<w?� 2 �� C�,u2��f���fQ��}��d �@O�0S)d�<����-|���)>�2�ݪ?�hP��ѲR�N��b��b��fD1HE��S
I
7*.���$�t�j��Q'E {a���QB
�r鋶��z�I�&�=��N^^\ri�j���:����-p�q:\u�C=k���r5��f�,wF�{/��~�����rĉ�<��qy������u��j���eDd�&�/�ºj���J�P.��b�/��R�+7��ʍr�F��Q� JS�W��(S uS�Wh�Q��(/F�=fu�8]�3e81��OY�	�����h.76      �      x������ � �      �   �  x�uRMo�0=3�����`K��1v�f�K�^z��⊆,/�~��y�x @���GFP(ul�+�Tmǖ�fQ�fI��y& Je����M��O�A�8���`au�86��FI���@�"\��2��D����B�V[���n�M�!C<4t�Ke�ϸ�nu��[��˕�Ld�-	OԶ����+�7����V�i��޵��� C�'�-��v����荪&؎W�{Ƃb.� C������X�bu�9�B����D�����"�i�AA�&OŃk�QU^�O
e���(K-�8��
9\��n���� �/ԑ�ᅎn��q��t��q���{��aݳ���_}q��'PAvZb�uǝ����GE+8�h�L�0�����ƽ��տ}WK!@�=Q�ҕo~�ڰɛժ�t�}C�B�N���b��j�ʌ      �   W   x�3���t�q�S 2��Px�(<μ���T����T��)VQ3���XE-��Zb54�.l�]���1va�4��CC�^4���=... 3t`�      �   L   x�3�T1JT10Q�,rJv1-5��J�IOH4+ԫ�6q2��6�H)��p1�t
1I����w������ ���      �     x�}�Ao�0���W��Uh�o�:!2��:���vN���9/�,y�O���G1"6E���S�QD�N�]'�,���������15��f��c����lS�M��"d�A�w���B�Oz�u�{��}�ݿ~����}�Y��GPZn�����s+�d��M����w��Y��z�䠊'��*8���}�9�~��Mk]���>4�j����l��]�����l�8�a�+��{j���v��RrD�sH��֜v��5��8�u7     