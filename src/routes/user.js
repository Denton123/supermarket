const { Router } = require('express');
const router = Router();



const {getusers,deleteuser, createuser,updateuser,getusers_code} = require('../controllers/user');
router.get('/users', getusers);
router.get('/user/:users_code', getusers_code);
router.post('/user/add',createuser);
router.delete('/user/:id',deleteuser);
router.put('/user/:user_id',updateuser);







module.exports = router;