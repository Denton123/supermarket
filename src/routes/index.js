const { Router } = require('express');
const router = Router();

const { getInventory,getInventory1,getInventory_code, createInventory, createInventory1,deleteinventory1, updateinventory1} = require('../controllers/index.controller');

router.get('/inventory2', getInventory1);



router.get('/inventory', getInventory);
router.get('/inventory/:inventory_id', getInventory_code);



// router.get('/users/:id', getUserById);
router.post('/inventory2', createInventory);
router.post('/inventory/add', createInventory1);

router.put('/inventory/:inventory_id', updateinventory1)
// router.delete('/inventory/:product_description', deleteinventory);
router.delete('/inventory/:id', deleteinventory1);
module.exports = router;