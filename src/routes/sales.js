

const { Router } = require('express');
const router = Router();


const {deletesale,getsale,updatesupplier,getsupplier_code} = require('../controllers/sales');

router.get('/viewsales', getsale);
router.delete("/sale/:id", deletesale);

module.exports = router;