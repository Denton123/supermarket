const { Router } = require('express');
const router = Router();


const { getdeliverytrans,createdeliverytrans,deletedelivery_transaction} = require('../controllers/delivery_trans');



router.get('/deliverytrans',getdeliverytrans);
router.post('/deliverytrans/add',createdeliverytrans);
router.delete("/deliverytrans/:id", deletedelivery_transaction);

module.exports = router;