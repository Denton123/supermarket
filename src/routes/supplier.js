const { Router } = require('express');
const router = Router();


const {getsupplier,createsupplier,updatesupplier,getsupplier_code,deletesupplier} = require('../controllers/supplier');



router.get('/supplier', getsupplier);
router.get('/supplier/:supplier_id', getsupplier_code);
router.post('/supplier/add', createsupplier);
router.put('/supplier/:supplier_id',updatesupplier);
router.delete('/supplier/:id',deletesupplier);



module.exports = router;