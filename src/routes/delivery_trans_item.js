const { Router } = require('express');
const router = Router();


const { getdeliveritem,createdeliveritem} = require('../controllers/delivery_trans_item');

// router.get('/delivery_transactions_items', getdeliveritem);
router.post('/delivery_transactions_items/add', createdeliveritem);


module.exports = router;