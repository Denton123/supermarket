const { Router } = require('express');
const router = Router();


const { createsales,deletesale,getsale} = require('../controllers/new_sales_transaction');

router.get('/sale',getsale);
router.post("/newsale/add",createsales)
router.delete("/sale/:id", deletesale);
module.exports = router;


