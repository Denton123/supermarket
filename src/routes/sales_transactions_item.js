const { Router } = require('express');
const router = Router();

const { getitem,createitem} = require('../controllers/sales_transactions_item');




router.get("/item",getitem);
router.post("/item/add",createitem)

module.exports = router;