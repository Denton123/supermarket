const { Router } = require("express");
const router = Router();

const {
  getrole,
  createrole,
  updaterole,
  deleterole,
  getrole_id,
  updaterole_user,
} = require("../controllers/role");
router.get("/role", getrole);
router.get("/role/:id", getrole_id);

router.post("/role/add", createrole);

router.delete("/role/:id", deleterole);
router.put("/role/:role_id", updaterole);
// router.put('/role/:roles_code',updaterole);
// router.put('/role/:id',updaterole_user);

module.exports = router;
