const { Pool } = require("pg");
const moment = require("moment");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "3031995",
  database: "lougeh_supermarket",
  port: "5432",
});

const getusers = async (req, res) => {
  // let response1 = await pool.query(
  //   "SELECT stransaction_code,supplier_code FROM delivery_transactions where dtransactions_code=$1",
  //   [stransaction_code]

  // Get all sales transaction
  const dt = await pool.query(
    `SELECT s.user_id,
      s.employee_code,
      s.username,
      s.password, 
      r.role_id,
      r.role_name,
      s.created_at,
      s.created_by,
      s.updated_by,
      s.updated_at
      FROM users s
      LEFT JOIN roles r ON s.role_id = r.role_id
      ORDER BY s.user_id;
 `
  );

  res.json(dt.rows);
};

//specific
const getusers_code = async (req, res) => {
  const user_id = parseInt(req.params.user_id);
  const response = await pool.query("SELECT * FROM users where user_id=$1", [
    user_id,
  ]);
  res.status(200).json(response.rows);
};

//sample
// const createuser = async (req, res) => {
//     const {username,password,employee_code,created_at,role_id} = req.body;

//     console.log('success');
//                     await pool.query('INSERT INTO users (username,password,employee_code,created_at,role_id) VALUES ($1, $2,$3,$4,$5)', [username,password,employee_code,created_at,role_id]);
//                     // await pool.query( 'SELECT role_name From users FULL JOIN roles ON role_id')
//     res.json({
//         message: 'New User Added successfully',
//         username,
//         password,
//         employee_code,
//         created_at,
//         role_id

//     })
// };

const createuser = async (req, res) => {
  const {
    password,
    username,
    employee_code,
    created_at,
    role_id,
    created_by,
  } = req.body;
  const today = moment().format("YYYY-MM-DD HH:mm:ss");

  // // SELECT SALT
  // // SELECT gen_salt('bf', 4)
  const res1 = await pool.query("SELECT gen_salt('bf', 4)");
  const salt = res1.rows[0].gen_salt;

  // // SELECT crypt ('mypass', $1), [salt];
  const res2 = await pool.query(`SELECT crypt('${password}', '${salt}')`);
  const encrypted_password = res2.rows[0].crypt;

  // const password // rsult ng select crypt
  // INSERT USER
  await pool.query(
    "INSERT INTO users (employee_code,username, password, role_id, created_by, created_at, salt) VALUES ($1, $2, $3, $4, $5, $6, $7)",
    [
      employee_code,
      username,
      encrypted_password,
      role_id,
      created_by,
      today,
      salt,
    ]
  );

  // await pool.query( 'SELECT role_name From users FULL JOIN roles ON role_id')
  res.json({
    message: "New User Added successfully",
    username,
    password,
    employee_code,
    created_at,
    role_id,
  });
};

const deleteuser = async (req, res) => {
  const user_id = parseInt(req.params.id);
  await pool.query("DELETE FROM users where user_id = $1", [user_id]);
  res.json(`user ${user_id} deleted Successfully`);
};

//update user
const updateuser = async (req, res) => {
  const user_id = parseInt(req.params.user_id);
  const { username, employee_code, password, role_id } = req.body;
  const response = await pool.query(
    "UPDATE users SET username = $1,employee_code=$2, password=$3,role_id=$4 WHERE user_id= $5 ",
    [username, employee_code, password, role_id, user_id]
  );
  res.json("role Updated Successfully");
};

module.exports = {
  getusers,
  createuser,
  deleteuser,
  updateuser,
  getusers_code,

  // deleterole,
  // updaterole
};
