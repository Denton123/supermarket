const { restart } = require("nodemon");
const { Pool } = require("pg");
const moment = require("moment");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "3031995",
  database: "lougeh_supermarket",
  port: "5432",
});


    const getsale = async (req, res) => {
        // const response = await pool.query('SELECT * FROM users ORDER BY id ASC');
          const response = await pool.query('SELECT * FROM sales_transactions,sales_transactions_items,inventory');
        
        res.status(200).json(response.rows);
    };
    
 



//create
// const createsales = async (req, res) => {
//     const { or_no,stransaction_date,barcode,quantity,unit_cost} = req.body;

//     let response1= await pool.query('INSERT INTO sales_transactions ( or_no,stransaction_date) VALUES ($1, $2)', 
//     [   or_no,
//         stransaction_date,

//     ]);
//     let response2=await pool.query('INSERT INTO sales_transactions_items ( barcode,quantity,or_no,unit_cost) VALUES ($1, $2,$3,$4)', 
//     [   barcode,
//         quantity,
//         or_no,
//         unit_cost
//     ])
  
//     res.json({
//         message: 'New sale transaction item Added successfully'
//     })

// };

const createsales = async (req, res) => {
    console.log(req.body);
    const { total_cost, payment_amt, customer_name, customer_address, customer_contact_no,items,or_no,created_by} = req.body;
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
  
    const st = await pool.query(
      "INSERT INTO sales_transactions (total_cost, payment_amt, customer_name, customer_address ,customer_contact_no, created_by,created_at,stransaction_date, or_no) VALUES ($1, $2, $3, $4, $5, $6,$7,$8, $9)",
      [ total_cost,payment_amt,customer_name,customer_address,customer_contact_no, created_by,today,today, or_no]
    );
  
    for (let i = 0; i < items.length; i++) {
      const { barcode, product_description, quantity, sales_cost } = items[i];
      if (!isNaN(barcode) && !isNaN(quantity) && !isNaN(sales_cost)) {
        await pool.query(
          `INSERT INTO sales_transactions_items (or_no, barcode, product_description, sales_cost, quantity) VALUES ($1, $2, $3, $4, $5) `,
          
          [or_no, barcode, product_description, sales_cost, quantity]
        );
        // await pool.query(
        //   'SELECT or_no as dt_no FROM sales_transactions_items'
        // );
        await pool.query(
          `UPDATE inventory SET quantity = quantity - $1 WHERE barcode = $2 `,
          [quantity, barcode]
        );
      }
  
    }
  
    res.json({
      message: "New sales transaction item Added successfully",
  
    });
  };
  

const deletesale = async (req, res) => {
    const stransaction_code = parseInt(req.params.id);
    await pool.query("DELETE FROM sales_transactions where stransaction_code = $1", [stransaction_code]);
    res.json(`role ${stransaction_code} deleted Successfully`);
  };



    module.exports={
        createsales,
        deletesale,
        getsale
    }