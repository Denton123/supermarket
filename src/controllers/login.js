const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

// const getlogin = async (req, res) => {
//     const role_code = parseInt(req.params.role_code);
//     const { role_name, role_id } = req.body;
//     const response = await pool.query(
//       "select roles SET role_name = $1, role_id=$2 WHERE role_code= $3 ",
//       [role_name, role_code, role_id]
//     );
//   console.log(response)
//     res.json({
//       message: "Role Updated Successfully",
//       role_name: role_name,
//       role_code: role_code,
//       role_id: role_id,
//     });
//   console.log(res)
//   };

module.exports = {
  getlogin,
};
