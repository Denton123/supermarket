const { response } = require("express");
const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "3031995",
  database: "lougeh_supermarket",
  port: "5432",
});

//specific
const getsale= async (req, res) => {
    const stransaction_code= parseInt(req.params.stransaction_code);
    // let response1 = await pool.query(
    //   "SELECT stransaction_code,supplier_code FROM delivery_transactions where dtransactions_code=$1",
    //   [stransaction_code]
 
    // Get all sales transaction
    const st = await pool.query(
      `SELECT st.st_id,
            st.or_no, 
            st.stransaction_date,
            st.total_cost,
            st.payment_amt,
            st.customer_name,
            st.customer_address,
            st.customer_contact_no,
            st.created_at,
            st.created_by
        FROM sales_transactions st
 `,);

    // Loop through all results
    for (let i = 0; i < st.rows.length; i++) {
      // Get sales transaction per index
      const element = st.rows[i];
      // Get all items based on OR number of current index
      const sti = await pool.query(
        ` SELECT sti.sti_id,
              sti.or_no,
              i.barcode,
              i.product_description, 
              sti.quantity,
              sti.sales_cost
              FROM sales_transactions_items sti
              LEFT JOIN inventory i ON sti.barcode = i.barcode 
          WHERE sti.or_no = $1
          ORDER BY sti.sti_id`,
        [element.or_no]
      );

      // Insert items on sales transaction based on index
      if (st.rows[i]) {
        st.rows[i].items = sti.rows;
      }
      
    }


 

    
  
    res.json(st.rows);
  };

  const deletesale = async (req, res) => {
    const stransaction_code = parseInt(req.params.id);
    await pool.query("DELETE FROM sales_transactions  where stransaction_code = $1", [stransaction_code]);
    res.json(`role ${stransaction_code} deleted Successfully`);

    let response1 = await pool.query("DELETE FROM sales_transactions_items  where stransaction_item_code = $1", [stransaction_code]);
    res.json(`sales ${stransaction_code} deleted Successfully`);
  };
  


module.exports= {
    getsale,
    deletesale


    
}