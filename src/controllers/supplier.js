const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

//get
const getsupplier = async (req, res) => {
  const response = await pool.query("SELECT * FROM suppliers");
  res.status(200).json(response.rows);
};

//specific
const getsupplier_code = async (req, res) => {
  const supplier_id = parseInt(req.params.supplier_id);
  const response = await pool.query(
    "SELECT * FROM suppliers where supplier_id=$1 ORDER BY supplier_id ",
    [supplier_id]
  );
  res.status(200).json(response.rows);
};

//add supplier
const createsupplier = async (req, res) => {
  const {
    company_name,
    contact_no,
    company_address,
    status,
    created_at,
    created_by,
  } = req.body;
  const response = await pool.query(
    "INSERT INTO suppliers ( company_name, contact_no,company_address,status,created_at,created_by) VALUES ($1, $2,$3,$4,$5,$6)",
    [company_name, contact_no, company_address, status, created_at, created_by]
  );
  res.json({
    message: "New supplier Added successfully",
    company_name,
    contact_no,
    company_address,
    status,
    created_at,
    created_by,
  });
};

//update supplier
const updatesupplier = async (req, res) => {
  const supplier_id = parseInt(req.params.supplier_id);
  const {
    company_name,
    contact_no,
    company_address,
    status,
    updated_by,
    updated_at,
  } = req.body;

  const response = await pool.query(
    "UPDATE suppliers SET company_name = $1,contact_no = $2 ,company_address=$3, status=$4 ,updated_at =$5,updated_by =$6 WHERE supplier_id= $7",
    [
      company_name,
      contact_no,
      company_address,
      status,
      updated_at,
      updated_by,
      supplier_id,
    ]
  );
  res.json("supplier Updated Successfully");
};

const deletesupplier = async (req, res) => {
  const supplier_id = parseInt(req.params.id);
  await pool.query("DELETE FROM suppliers where supplier_id = $1", [
    supplier_id,
  ]);
  res.json(`spplier ${supplier_id} deleted Successfully`);
};

module.exports = {
  getsupplier,
  createsupplier,
  updatesupplier,
  getsupplier_code,
  deletesupplier,
};
