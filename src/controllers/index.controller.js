const { Pool } = require("pg");
const moment = require("moment");
const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});
//get
const getInventory = async (req, res) => {
  // const response = await pool.query('SELECT * FROM users ORDER BY id ASC');
  const response = await pool.query("SELECT * FROM inventory ");

  res.status(200).json(response.rows);
};
//specefic
const getInventory_code = async (req, res) => {
  const inventory_id = parseInt(req.params.inventory_id);
  const response = await pool.query(
    "SELECT * FROM inventory where inventory_id=$1",
    [inventory_id]
  );
  res.status(200).json(response.rows);
};

//
const getInventory1 = async (req, res) => {
  const response = await pool.query("SELECT * FROM inventory2,roles,users");

  res.status(200).json(response.rows);
};
//table2
const createInventory = async (req, res) => {
  const { barcode, product_description, quantity } = req.body;
  const response = await pool.query(
    "INSERT INTO inventory2 (barcode, product_description, quantity) VALUES ($1, $2, $3)",
    [barcode, product_description, quantity]
  );
  res.json({
    message: "User Added successfully",
  });
};
//inventory
const createInventory1 = async (req, res) => {
  console.log;
  const {
    barcode,
    product_description,
    quantity,
    unit_cost,
    sales_cost,
    created_at,
    created_by,
  } = req.body;
  const response = await pool.query(
    "INSERT INTO inventory (barcode, product_description, quantity,unit_cost,sales_cost,created_at,created_by) VALUES ($1, $2,$3,$4,$5,$6,$7)",
    [
      barcode,
      product_description,
      quantity,
      unit_cost,
      sales_cost,
      created_at,
      created_by,
    ]
  );
  res.json({
    message: "New Product Added successfully",
    barcode,
    product_description,
    quantity,
    unit_cost,
    sales_cost,
    created_at,
    created_by,
  });
};

const deleteinventory1 = async (req, res) => {
  const inventory_id = parseInt(req.params.id);
  await pool.query("DELETE FROM inventory where inventory_id = $1", [
    inventory_id,
  ]);
  res.json(`inventory ${inventory_id} deleted Successfully`);
};

const updateinventory1 = async (req, res) => {
  const inventory_id = parseInt(req.params.inventory_id);
  const {
    product_description,
    quantity,
    unit_cost,
    sales_cost,
    updated_at,
    updated_by,
  } = req.body;
  const today = moment().format("YYYY-MM-DD HH:mm:ss");

  const response = await pool.query(
    "UPDATE inventory SET product_description = $1,quantity = $2 ,unit_cost=$3,sales_cost=$4, updated_at =$5,updated_by =$6 WHERE inventory_id = $7",
    [
      product_description,
      quantity,
      unit_cost,
      sales_cost,
      today,
      updated_by,
      inventory_id,
    ]
  );
  res.json("Inventory Updated Successfully");
};

// const updateUser = async (req, res) => {
//     const id = parseInt(req.params.id);
//     const { name, email } = req.body;

//     const response =await pool.query('UPDATE users SET name = $1, email = $2 WHERE id = $3', [
//         name,
//         email,
//         id
//     ]);
//     res.json('User Updated Successfully');
// };

// const deleteUser = async (req, res) => {
//     const id = parseInt(req.params.id);
//     await pool.query('DELETE FROM users where id = $1', [
//         id
//     ]);
//     res.json(`User ${id} deleted Successfully`);
// };

module.exports = {
  getInventory,
  updateinventory1,
  createInventory,
  createInventory1,
  deleteinventory1,
  getInventory1,
  getInventory_code,
};
