const { Pool } = require("pg");
const moment = require("moment");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "3031995",
  database: "lougeh_supermarket",
  port: "5432",
});

//get
const getdeliverytrans = async (req, res) => {
  // const response = await pool.query('SELECT * FROM users ORDER BY id ASC');
  const response = await pool.query("SELECT * FROM delivery_transactions ");

  res.status(200).json(response.rows);
};

//create
const createdeliverytrans = async (req, res) => {
  console.log(req.body);
  const {
    supplier_id,
    total_cost,
    dr_no,
    dt_no,
    items,
    created_by,
  } = req.body.transaction;
  console.log(items);
  const today = moment().format("YYYY-MM-DD HH:mm:ss");

  const dt = await pool.query(
    "INSERT INTO delivery_transactions (dr_no, supplier_id, dtransaction_date, total_cost ,created_at, created_by) VALUES ($1, $2, $3, $4, $5, $6)",
    [dr_no, supplier_id, today, total_cost, today, created_by]
  );

  for (let i = 0; i < items.length; i++) {
    const { barcode, product_description, quantity, cost_per_unit } = items[i];
    await pool.query(
      `INSERT INTO delivery_transactions_items (dt_no, barcode, product_description, unit_cost, quantity) VALUES ($1, $2, $3, $4, $5) `,
      [dt_no, barcode, product_description, cost_per_unit, quantity]
    );
    await pool.query(
      `UPDATE inventory SET quantity = quantity + $1 WHERE barcode = $2 `,
      [quantity, barcode]
    );
  }

  res.json({
    message: "New delivery transaction item Added successfully",
  });
};

const deletedelivery_transaction = async (req, res) => {
  const dtransactions_code = parseInt(req.params.id);
  await pool.query(
    "DELETE FROM delivery_transactions where dtransactions_code= $1",
    [dtransactions_code]
  );
  res.json(`delivery Transactions ${dtransactions_code} deleted Successfully`);
};

module.exports = {
  getdeliverytrans,
  createdeliverytrans,
  deletedelivery_transaction,
};
