const { restart } = require("nodemon");
const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

const getdeliveritem = async (req, res) => {
  // const response = await pool.query('SELECT * FROM users ORDER BY id ASC');
  const response = await pool.query(
    "SELECT * FROM delivery_transactions_items "
  );

  res.status(200).json(response.rows);
};

//create
const createdeliveritem = async (req, res) => {
  const {
    barcode,
    unit_cost,
    quantity,
    items,
    supplier_id,
    dtransaction_date,
  } = req.body;

  let response1 = await pool.query(
    `INSERT INTO public.delivery_transactions(
           dtransaction_date, total_cost, created_at, updated_by, updated_at)
        VALUES ( $1, $2, $3, $4, $5) returning dt_no, dtransactions_code;`,
    [dtransaction_date, 0, dtransaction_date, 1, dtransaction_date]
  );

  let total_cost = 0;

  for (let i = 0; i < items.length; i++) {
    const item_cost = items[i].cost_per_unit * items[i].quantity;

    total_cost = total_cost + item_cost;
    let response2 = await pool.query(
      `INSERT INTO public.delivery_transactions_items(
         dt_no, barcode, unit_cost, quantity)
        VALUES ($1, $2, $3, $4);`,
      [
        response1.rows[0].dt_no,
        items[i].barcode,
        items[i].cost_per_unit,
        items[i].quantity,
      ]
    );
  }

  let response3 = await pool.query(
    `UPDATE public.delivery_transactions
      SET total_cost=$1
      WHERE dtransactions_code = $2;`,
    [total_cost, response1.rows[0].dtransactions_code]
  );

  res.json({
    message: "New delivery transaction item Added successfully",
  });
};

module.exports = {
  getdeliveritem,
  createdeliveritem,
};
