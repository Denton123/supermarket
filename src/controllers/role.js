const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

//get
// const getrole = async (req, res) => {
//   const response = await pool.query("SELECT role_id FROM roles FULL OUTER JOIN users on role_id = username");
//   res.status(200).json(response.rows);
// };

const getrole = async (req, res) => {
  const response = await pool.query("SELECT * FROM roles");
  res.status(200).json(response.rows);
};

//get specefic
const getrole_id = async (req, res) => {
  const role_id = parseInt(req.params.id);
  const response = await pool.query("SELECT * FROM roles where role_id=$1", [
    role_id,
  ]);
  res.status(200).json(response.rows);
};
//add (post)
// const createrole = async (req, res) => {
//     const { role_name, created_at,created_by,updated_at } = req.body;
//     console.log('success');
//     const response = await pool.query('INSERT INTO roles (role_name, created_at, created_by,updated_at ) VALUES ($1, $2, $3,$4)', [role_name, created_at,created_by,updated_at]);
//     res.json({
//         message: 'New Role Added successfully',

//     })
// };

const createrole = async (req, res) => {
  const { role_name } = req.body;

  console.log("success");
  await pool.query("INSERT INTO roles (role_name) VALUES ($1)", [role_name]);

  res.json({
    message: "New role Added successfully",
  });
};
//delete
const deleterole = async (req, res) => {
  const role_id = parseInt(req.params.id);
  await pool.query("DELETE FROM roles where role_id= $1", [role_id]);
  res.json(`role ${role_id} deleted Successfully`);
};

const updaterole = async (req, res) => {
  const role_id = parseInt(req.params.role_id);
  const { role_name } = req.body;
  const response = await pool.query(
    "UPDATE roles SET role_name = $1  WHERE role_id= $2 ",
    [role_name, role_id]
  );
  res.json("role Updated Successfully");
};

module.exports = {
  getrole,
  createrole,
  deleterole,
  updaterole,
  getrole_id,
  // updaterole_user
};
