const { response } = require("express");
const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

//specific
const gettest = async (req, res) => {
  const st_id = parseInt(req.params.st_id);
  // let response1 = await pool.query(
  //   "SELECT st_id,supplier_id FROM delivery_transactions where dtransactions_code=$1",
  //   [st_id]

  // Get all sales transaction
  const dt = await pool.query(
    `SELECT dt.dtransactions_code,
            dt.dt_no, 
           s.supplier_id,
            s.company_name,
            s.company_address,
            dt.dtransaction_date,
           dt.total_cost,
            s.created_at,
            s.created_by,
			dt.updated_by,
			dt.updated_at
        FROM delivery_transactions dt 
		LEFT JOIN suppliers s on dt.dtransactions_code = s.supplier_id
 `
  );

  // Loop through all results
  for (let i = 0; i < dt.rows.length; i++) {
    // Get sales transaction per index
    const element = dt.rows[i];
    // Get all items based on OR number of current index
    const dti = await pool.query(
      `  SELECT dti.dti_id,
        dti.dt_no,
         i.barcode,
         i.product_description, 
         dti.unit_cost,
         dti.quantity
         FROM delivery_transactions_items dti
         LEFT JOIN inventory i ON dti.barcode = i.barcode 
     WHERE dti.dt_no = $1
     ORDER BY dti.dti_id`,
      [element.dt_no]
    );

    // Insert items on sales transaction based on index
    if (dt.rows[i]) {
      dt.rows[i].items = dti.rows;
    }
  }

  res.json(dt.rows);
};

module.exports = {
  gettest,
};
