const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

const getitem = async (req, res) => {
  const response = await pool.query("SELECT * FROM sales_transactions_items");
  res.status(200).json(response.rows);
};

const createitem = async (req, res) => {
  const { or_no, barcode, quantity, unit_cost } = req.body;
  const response = await pool.query(
    "INSERT INTO sales_transactions_items ( or_no,barcode,quantity,unit_cost) VALUES ($1, $2,$3,$4)",
    [or_no, barcode, quantity, unit_cost]
  );
  res.json({
    message: "New  item Added successfully",
    or_no,
    barcode,
    quantity,
    unit_cost,
  });
};

module.exports = {
  getitem,
  createitem,
};
