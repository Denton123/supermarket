const { response } = require("express");
const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "3031995",
  database: "lougeh_supermarket",
  port: "5432",
});

//specific
const gettest= async (req, res) => {
    const stransaction_code= parseInt(req.params);
    // Get all sales transaction
    const  dt = await pool.query(
      `SELECT dt.dtransactions_code,
	dt.dr_no, 
	s.company_name as supplier_name,
	dt.dtransaction_date as transaction_date, 
	dt.total_cost
FROM delivery_transactions dt
LEFT JOIN suppliers s ON s.supplier_id = dt.supplier_id

    ORDER BY dt.dtransactions_code
 `,);

    // Loop through all results
    for (let i = 0; i < dt.rows.length; i++) {
      // Get sales transaction per index
      const element = dt.rows[i];
      // Get all items based on OR number of current index
      const dti = await pool.query(
        `  SELECT dti.dti_id,
        dti.dt_no,
         i.barcode,
         i.product_description, 
         dti.unit_cost,
         dti.quantity
         FROM delivery_transactions_items dti
         LEFT JOIN inventory i ON dti.barcode = i.barcode 
     WHERE dti.dt_no = $1
     ORDER BY dti.dti_id
     `,
        [element.dr_no]
      );

      // Insert items on sales transaction based on index
      if (dt.rows[i]) {
        dt.rows[i].items = dti.rows;
      }
      
    }


   

    

  
    res.json(dt.rows);
  };




module.exports= {
  gettest


    
}