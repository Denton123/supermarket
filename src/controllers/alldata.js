const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

//get
const getalldata = async (req, res) => {
  const response = await pool.query("SELECT * FROM roles, users");
  res.status(200).json(response.rows);
};

module.exports = {
  getalldata,
};
