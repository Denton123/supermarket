const { Pool } = require("pg");

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  password: "postgres",
  database: "lougeh_supermarket",
  port: "5433",
});

const getsales = async (req, res) => {
  const response = await pool.query(
    "SELECT * FROM inventory,sales_transactions,sales_transactions_items"
  );
  res.status(200).json(response.rows);
};

const creatsales = async (req, res) => {
  const {
    or_no,
    stransaction_date,
    total_cost,
    payment_amt,
    customer_name,
    customer_address,
    customer_contact_no,
    created_at,
    created_by,
  } = req.body;

  console.log("success");
  await pool.query(
    "INSERT INTO sales_transactions (or_no,stransaction_date,total_cost,payment_amt,customer_name,customer_address,customer_contact_no,created_at,created_by) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)",
    [
      or_no,
      stransaction_date,
      total_cost,
      payment_amt,
      customer_name,
      customer_address,
      customer_contact_no,
      created_at,
      created_by,
    ]
  );

  res.json({
    message: "New sales_transactions Added successfully",
    or_no,
    stransaction_date,
    total_cost,
    payment_amt,
    customer_name,
    customer_address,
    customer_contact_no,
    created_at,
    created_by,
  });
};

module.exports = {
  getsales,
  creatsales,
};
