const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

const cors = require("cors");
app.use(cors());

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use(require('./routes/index'));
app.use(require('./routes/role'));
app.use(require('./routes/user'));
app.use(require('./routes/supplier'));
app.use(require('./routes/delivery_trans_item'));
app.use(require('./routes/delivery_trans'));
app.use(require('./routes/delivery'));
app.use(require('./routes/sales_transactions'));
app.use(require('./routes/sales_transactions_item'));
app.use(require('./routes/login'));
app.use(require('./routes/new_sales_transaction'));
app.use(require('./routes/test'));
app.use(require('./routes/sales'));
















app.listen(5000);
console.log('Server on port', 5000);